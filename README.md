Channelizer Server
==================

This is a server implemented in rust that will receive the data from a
channelizer via DMA transfers and then push out the deinterleaved channels via
ZMQ pub/sub with the channel indexes as the publisher topics.

Installation
============

Obviously you need to have [rust](https://www.rust-lang.org/en-US/install.html)
installed to build the code.  Additionally libzmq needs to be installed (this
is libzmq3-dev on ubuntu.)  The code is built with a standard:

``` sh
$ cargo build
```
