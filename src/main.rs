#![crate_name = "chan_server"]

extern crate zmq;

use std::env;
use std::thread;
use std::time::{Duration, Instant};
use std::sync::mpsc::{channel, Sender, Receiver};


const PKT_BYTES: u32 = 0x800;


// Given input I/Q sample data, select a slice associated with an indicated
// channel index.
fn get_chan<'a>(data: &'a Vec<u8>, chan_idx: u32) -> &'a [u8] {
    let start = (chan_idx * PKT_BYTES) as usize;
    let stop = ((chan_idx + 1) * PKT_BYTES) as usize;
    &data[start..stop]
}


fn u16_to_u8_slice(x: u16) -> [u8; 2] {
    let b1: u8 = ((x >> 8) & 0xff) as u8;
    let b2: u8 = (x & 0xff) as u8;
    return [b1, b2];
}


fn publish(data: Vec<u8>, n_chans: u32) {
    let context = zmq::Context::new();
    let publisher = context.socket(zmq::PUB).unwrap();
    publisher.bind("tcp://*:5563").expect("failed binding publisher");

    for i in 0 .. n_chans {
        let slice = get_chan(&data, i);
        let u16_topic: u16 = i as u16;
        let topic_bytes = u16_to_u8_slice(u16_topic);
        publisher.send(&topic_bytes, zmq::SNDMORE)
                 .expect("failed sending topic");
        publisher.send(&slice, 0)
                 .expect("failed sending data");
    }
}


fn subscribe(chan: u32) {
    let context = zmq::Context::new();
    let subscriber = context.socket(zmq::SUB).unwrap();
    subscriber.connect("tcp://localhost:5563")
              .expect("failed connecting subscriber");

    let u16_topic: u16 = chan as u16;
    let topic_bytes = u16_to_u8_slice(u16_topic);
    subscriber.set_subscribe(&topic_bytes)
              .expect("failed subscribing");

    loop {
        let envelope = subscriber.recv_bytes(0)
            .expect("failed receiving envelope");
        let message = subscriber.recv_bytes(0)
            .expect("failed receiving message");
        println!("[{:#?}] {:#?}", envelope, message);
    }
}


fn server(pull_socket: zmq::Socket,
          push_socket: zmq::Socket,
          mut workers: u64) {
    let mut count = 0;

    while workers != 0 {
        let s = pull_socket.recv_string(0).unwrap().unwrap();
        if s.is_empty() {
            workers -= 1;
        } else {
            count += s.parse::<u32>().unwrap();
        }
    }

    push_socket.send_str(&count.to_string(), 0).unwrap();
}


fn spawn_server(ctx: &mut zmq::Context, workers: u64) -> Sender<()> {
    let pull_socket = ctx.socket(zmq::PULL).unwrap();
    let push_socket = ctx.socket(zmq::PUSH).unwrap();

    pull_socket.bind("inproc://server-pull").unwrap();
    push_socket.bind("inproc://server-push").unwrap();

    // Spawn the server
    let (ready_tx, ready_rx) = channel();
    let (start_tx, start_rx) = channel();

    thread::spawn(move|| {
        // Let the main thread know we're ready
        ready_tx.send(()).unwrap();

        // Wait until we need to start
        start_rx.recv().unwrap();

        server(pull_socket, push_socket, workers);
    });

    // Wait for the server to start
    ready_rx.recv().unwrap();

    start_tx
}


fn worker(push_socket: zmq::Socket, count: u64) {
    for _ in 0 .. count {
        push_socket.send_str(&100.to_string(), 0).unwrap();
    }

    // Let the server know we're done
    push_socket.send(b"", 0).unwrap();
}


fn spawn_worker(ctx: &mut zmq::Context, count: u64) -> Receiver<()> {
    let push_socket = ctx.socket(zmq::PUSH).unwrap();

    push_socket.connect("inproc://server-pull").unwrap();

    // Spawn the worker
    let (tx, rx) = channel();
    thread::spawn(move|| {
        // Let the main thread know we're ready
        tx.send(()).unwrap();

        worker(push_socket, count);

        tx.send(()).unwrap();
    });

    // Wait for the worker to start
    rx.recv().unwrap();

    rx
}


fn seconds(d: &Duration) -> f64 {
    d.as_secs() as f64 + (d.subsec_nanos() as f64 / 1e9)
}


fn run(ctx: &mut zmq::Context, size: u64, workers: u64) {
    let start_ch = spawn_server(ctx, workers);

    // Create some command/control sockets
    let push_socket = ctx.socket(zmq::PUSH).unwrap();
    let pull_socket = ctx.socket(zmq::PULL).unwrap();

    push_socket.connect("inproc://server-pull").unwrap();
    pull_socket.connect("inproc://server-push").unwrap();

    // Spawn all the workers
    let mut worker_results = Vec::new();
    for _ in 0 .. workers {
        worker_results.push(spawn_worker(ctx, size / workers));
    }

    let start = Instant::now();

    start_ch.send(()).unwrap();

    // Block until all the workers finish
    for po in worker_results {
        po.recv().unwrap();
    }

    // Receive the final count
    let msg = pull_socket.recv_msg(0).unwrap();
    let result = msg.as_str().unwrap().parse::<i32>().unwrap();

    let elapsed = seconds(&start.elapsed());

    println!("Count is {}", result);
    println!("Test took {} seconds", elapsed);
    let throughput = ((size / workers * workers) as f64) / elapsed;
    println!("Throughput={} per sec", throughput);
}


fn main() {
    let args: Vec<String> = env::args().collect();

    let args = if env::var("RUST_BENCH").is_ok() {
        vec!("".to_string(), "1000000".to_string(), "10000".to_string())
    } else if args.len() <= 1 {
        vec!("".to_string(), "10000".to_string(), "4".to_string())
    } else {
        args
    };

    let size = args[1].parse().unwrap();
    let workers = args[2].parse().unwrap();

    let mut ctx = zmq::Context::new();

    run(&mut ctx, size, workers);

    /*
    thread::spawn(|| {
        publish();
    });

    subscribe();
    */
}
